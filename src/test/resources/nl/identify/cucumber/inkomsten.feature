# language: nl
Functionaliteit: Inkomsten

  Scenario: 1 Voer inkomsten uit loondienst op
    Gegeven een leeg hypotheekdossier
    Als de klant navigeert naar lamel inkomsten
    En inkomen vaste loondienst met jaarinkomen "40000" opvoert
    En klikt op Reken met deze bedragen
    Dan wordt het overzichtsscherm scherm getoond
    En is de inkomsten lamel compleet
    En is het opgegeven inkomen correct overgenomen in het overzichtsscherm
