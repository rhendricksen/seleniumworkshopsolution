package nl.identify.cucumber.defs;

import cucumber.api.PendingException;
import cucumber.api.java.nl.Als;
import cucumber.api.java.nl.Dan;
import cucumber.api.java.nl.En;
import cucumber.api.java.nl.Gegeven;
import nl.identify.cucumber.pages.InkomstenPage;
import nl.identify.cucumber.pages.LandingPage;
import nl.identify.cucumber.pages.PersoonlijkeBerekeningPage;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class InkomstenDefs {

    private LandingPage landingPage = new LandingPage();
    private PersoonlijkeBerekeningPage persoonlijkeBerekeningPage = new PersoonlijkeBerekeningPage();
    private InkomstenPage inkomstenPage = new InkomstenPage();

    @Gegeven("^een leeg hypotheekdossier$")
    public void eenLeegHypotheekdossier() {
        landingPage.openLandingPage()
                .setKoopWoning(false)
                .clickNaarUwPersoonlijkeHypotheekDossier()
                .clickStartMetUwPersoonlijkeBerekening();
    }

    @Als("^de klant navigeert naar lamel inkomsten$")
    public void deKlantNavigeertNaarLamelInkomsten() {
        persoonlijkeBerekeningPage.openInkomsten();
    }

    @En("^inkomen vaste loondienst met jaarinkomen \"([^\"]*)\" opvoert$")
    public void inkomenVasteLoondienstMetJaarinkomenOpvoert(String inkomen) {
        inkomstenPage.setInkomstenAanvrager()
                .setContractVast()
                .setInkomen(inkomen)
                .setTermijn("jaar")
                .clickBevestingInkomen()
                .setVervroegdMetPensioen(false);
    }

    @En("^klikt op Reken met deze bedragen$")
    public void kliktOpRekenMetDezeBedragen() {
        inkomstenPage.clickRekenMetDezeBedragen();
    }

    @Dan("^wordt het overzichtsscherm scherm getoond$")
    public void wordtHetOverzichtsschermSchermGetoond() {
        assertThat (persoonlijkeBerekeningPage.isInkomstenButtonVisible(), is(true));
    }

    @En("^is de inkomsten lamel compleet$")
    public void isDeInkomstenLamelCompleet() throws Throwable {
        assertThat (persoonlijkeBerekeningPage.isInkomstenStatusComplete(), is(true));
    }

    @En("^is het opgegeven inkomen correct overgenomen in het overzichtsscherm$")
    public void isHetOpgegevenInkomenCorrectOvergenomenInHetOverzichtsscherm() {
        assertThat (persoonlijkeBerekeningPage.getMaxLenenObvInkomen(), is("€ 174.300"));
        assertThat (persoonlijkeBerekeningPage.getBrutoJaarInkomen(), is("€ 43.200"));
    }
}
