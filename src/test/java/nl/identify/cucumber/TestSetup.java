package nl.identify.cucumber;

import com.codeborne.selenide.Configuration;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class TestSetup {

    public static final int MAX_WAIT_TME = 10000;

    @Before
    public void buildUp(final Scenario scenario) {
        Configuration.holdBrowserOpen = true;

    }

    @After
    public void tearDown(final Scenario scenario) {

    }
}
