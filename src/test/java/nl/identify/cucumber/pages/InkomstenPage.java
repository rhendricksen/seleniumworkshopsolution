package nl.identify.cucumber.pages;

import static com.codeborne.selenide.Selenide.$;
import static org.openqa.selenium.By.name;

public class InkomstenPage {

    public InkomstenPage setInkomstenAanvrager() {
        $("#moa-module-income-overview-specselector-button-addspec-mainPerson").click();
        return this;
    }

    public InkomstenPage setContractVast() {
        $("#moa-module-income-specselector-radio-perm").click();
        return this;
    }

    public InkomstenPage clickBevestingInkomen() {
        $("#moa-module-income-specification-sidebar-submit-button").click();
        return this;
    }

    public InkomstenPage setInkomen(String inkomen) {
        $("#moa-module-income-specselector-perm-salary-amount").val(inkomen);
        return this;
    }

    public InkomstenPage setTermijn (String termijn) {
        $("#moa-module-income-specselector-perm-salary-period").selectOption(termijn);
        return this;
    }

    public InkomstenPage setVervroegdMetPensioen (boolean pensioen) {
        $(name("moa-mortgage-boolean-input-mainPersonFutureIncome")).selectRadio(String.valueOf(pensioen));
        return this;
    }

    public PersoonlijkeBerekeningPage clickRekenMetDezeBedragen() {
        $("#moa-module-sidebar-submit-button").click();
        return new PersoonlijkeBerekeningPage();
    }
}
