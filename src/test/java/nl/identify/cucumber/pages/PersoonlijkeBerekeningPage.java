package nl.identify.cucumber.pages;

import com.codeborne.selenide.Condition;
import static com.codeborne.selenide.Selenide.$;
import static nl.identify.cucumber.TestSetup.MAX_WAIT_TME;

public class PersoonlijkeBerekeningPage {

    public PersoonlijkeBerekeningPage clickStartMetUwPersoonlijkeBerekening() {
        $("#moa-blind-persoonlijkeberekening").click();
        return this;
    }

    public InkomstenPage openInkomsten() {
        $("#moa-income-button").click();
        return new InkomstenPage();
    }

    public boolean isInkomstenStatusComplete() {
        $(".moa-button.moa-income-button.moa-bh-icon-symbol.status-complete").waitUntil(Condition.exist, MAX_WAIT_TME);
        return true;
    }

    public String getMaxLenenObvInkomen() {
        return $("#maxTeLenenInkomen-amount").text();
    }

    public String getBrutoJaarInkomen() {
        return $("#moa-module-main-sidebar-summary-totaalBrutoJaarinkomen-value").text();
    }

    public boolean isInkomstenButtonVisible() {
        return $("#moa-income-button").isDisplayed();
    }
}
