package nl.identify.cucumber.pages;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static org.openqa.selenium.By.name;

public class LandingPage {

    public LandingPage openLandingPage() {
        open("https://bankieren.rabobank.nl/hypotheken/");
        return this;
    }

    public LandingPage setKoopWoning(boolean hasKoopWoning) {
        $(name("moa-koopwoning")).selectRadio(String.valueOf(hasKoopWoning));
        return this;
    }

    public PersoonlijkeBerekeningPage clickNaarUwPersoonlijkeHypotheekDossier() {
        $("#moa-choice-kbg-submit").click();
        return new PersoonlijkeBerekeningPage();
    }
}
